package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * HomePage is a page object class that represents the home page of the web application.
 * It extends BasePage and provides methods to interact with elements on the home page.
 */
public class HomePage extends BasePage {
    public static final String SEARCH_BUTTON = "btn-global-search";
    public static final String SEARCH_INPUT = "input-global-search";
    public static final String LOGIN_BUTTON_NAV = "navLoginBtn";
    public static final String TRIAL_BUTTON_NAV = "//*[@id='container']//li[9]/a";
    public static final String CONTACT_US_NAV = "//*[@id='container']//li[8]/a";

    @FindBy(id = SEARCH_BUTTON)
    private WebElement searchButton;

    public WebElement getSearchButton() {
        return searchButton;
    }

    @FindBy(id = SEARCH_INPUT)
    private WebElement searchInput;

    public WebElement getSearchInput() {
        return searchInput;
    }
    @FindBy(id = LOGIN_BUTTON_NAV)
    private WebElement loginButtonNav;

    public WebElement getLoginButtonNav() {
        return loginButtonNav;
    }

    @FindBy(xpath = TRIAL_BUTTON_NAV)
    private WebElement trialButtonNav;

    public WebElement getTrialButtonNav() {
        return trialButtonNav;
    }

    @FindBy(xpath = CONTACT_US_NAV)
    private WebElement contactUsButtonNav;

    public WebElement getContactUsButtonNav() {
        return contactUsButtonNav;
    }
}
