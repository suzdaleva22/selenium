package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * ContactUsPage is a page object class that represents the contact us page of the web application.
 * It extends BasePage and provides methods to interact with the elements on the contact us page.
 */
public class ContactUsPage  extends BasePage{

    public static final String CONTACT_INFO_GERMANY = "//div[5]/h2";

    @FindBy(xpath = CONTACT_INFO_GERMANY)
    private WebElement contactInfoGe;

    public WebElement getContactInfoGe() {

        return contactInfoGe;
    }

}