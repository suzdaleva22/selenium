package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * TrialSignUpPage is a page object class that represents the trial sign-up page of the web application.
 * It extends BasePage and provides methods to interact with elements on the trial sign-up page.
 */
public class TrialSignUpPage extends BasePage{
    public static final String TRIAL_SIGN_UP_FORM = "frmRegister";

    @FindBy(css = TRIAL_SIGN_UP_FORM)
    private WebElement trialSignUpForm;

    public WebElement getTrialSignUpForm() {

        return trialSignUpForm;
    }
}