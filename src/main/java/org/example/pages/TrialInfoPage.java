package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * TrialInfoPage is a page object class that represents the trial information page of the web application.
 * It extends BasePage and provides methods to interact with elements on the trial information page.
 */
public class TrialInfoPage extends BasePage{
    public static final String TRIAL_GO_BUTTON = "//*[text()='Go']";

    @FindBy(xpath = TRIAL_GO_BUTTON)
    private WebElement trialGoButton;

    public WebElement getTrialGoButton() {
        return trialGoButton;
    }

}