package org.example.pages;

import org.example.utils.Driver;
import org.openqa.selenium.support.PageFactory;
/**
 * BasePage is an abstract class that serves as a foundation for all page objects in the web application.
 * It initializes web elements using the PageFactory and provides common functionality for all derived pages.
 */

abstract public class BasePage {
    public BasePage() {
        PageFactory.initElements(Driver.get(), this);
    }
}
