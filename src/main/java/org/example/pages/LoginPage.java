package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * LoginPage is a page object class that represents the login page of the web application.
 * It extends BasePage and provides methods to interact with elements on the login page.
 */
public class LoginPage extends BasePage{
    public static final String LOGIN_FORM = "//form[@id='frmAspNet";
    public static final String LOGIN_SUBMIT_BUTTON = "cplMainContent_LoginUser_btnLogIn";

    @FindBy(xpath = LOGIN_FORM)
    private WebElement loginForm;

    public WebElement getLoginForm() {
        return loginForm;
    }

    @FindBy(id = LOGIN_SUBMIT_BUTTON)
    private WebElement loginSubmitButton;

    public WebElement getLoginSubmitButton() {
        return loginSubmitButton;
    }
}