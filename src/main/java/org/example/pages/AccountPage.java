package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * AccountPage is a page object class that represents the account page of the web application.
 * It extends BasePage and provides methods to interact with the elements on the account page.
 */
public class AccountPage extends BasePage {

    public static final String PAGE_TITLE = "//*[@id='frmAspNet']/h1";

    @FindBy(xpath = PAGE_TITLE)
    private WebElement pageTitle;

    public WebElement getPageTitle() {
        return pageTitle;
    }
}
