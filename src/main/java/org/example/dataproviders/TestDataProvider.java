package org.example.dataproviders;

import java.util.HashMap;
import java.util.Map;

/**
 * test data for login
 */
public class TestDataProvider {
    private static Map<String, String> testUserData = createUserTest();
    public static Map<String, String> createUserTest() {
        Map<String, String> testUser = new HashMap<>();
        testUser.put("userName", "Yuliya Suzdalyeva");
        testUser.put("email", "suzdalyeva22@gmail.com");
        testUser.put("password", "n2!NMC8qCKWd7Z.");
        return testUser;
    }
    public static String getTestUserData(String dataName) {
        return testUserData.get(dataName);
    }
}

