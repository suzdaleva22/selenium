
package org.example.helpers;

import org.openqa.selenium.WebDriver;
/**
 * HomePageHelper is a helper class that provides methods specific to interactions
 * with the home page of the web application. It extends BasePageHelper to
 * inherit common methods for interacting with web elements.
 */
public class HomePageHelper extends BasePageHelper{
    WebDriver driver;
    public HomePageHelper(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
}
