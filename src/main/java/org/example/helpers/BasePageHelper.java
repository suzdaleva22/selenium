package org.example.helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/**
 * BasePageHelper is a base class for common page helper methods
 */
public class BasePageHelper {
    WebDriver driver;
    public BasePageHelper (WebDriver driver) {
        this.driver = driver;

    }
}


