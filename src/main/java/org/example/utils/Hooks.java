package org.example.utils;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
/**
 * The Hooks class contains setup and teardown methods to be executed before and after test classes.
 * It utilizes TestNG annotations to manage test execution lifecycle.
 */
public class Hooks {
    @BeforeClass
    public void setUp(){
        Driver.get().manage().window().maximize();
    }
    @AfterClass
    public void tearDown(){
        Driver.closeDriver();
    }

}
