
import org.example.pages.*;
import org.example.utils.BrowserUtils;
import org.example.utils.ConfigurationReader;
import org.example.utils.Driver;
import org.example.utils.Hooks;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.testng.annotations.Test;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.example.dataproviders.TestDataProvider.getTestUserData;
import static org.testng.Assert.assertTrue;

/**
 * class with tests for the site https://www.inflectra.com/
 */
public class InflectraTest extends Hooks {

    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    AccountPage accountPage = new AccountPage();
    TrialInfoPage trialInfoPage = new TrialInfoPage();
    TrialSignUpPage trialSignUpPage = new TrialSignUpPage();
    ContactUsPage contactUsPage = new ContactUsPage();

    /**
     * We use this method for the setting the page URL
     * @param page
     */
    public void setUrl(String page){
        String url = ConfigurationReader.get(page);
        Driver.get().get(url);
    }
    /**
     * The testHomePage() method verifies the title of the home page after navigating to the Inflectra URL.
     */
    @Test
    public void testHomePage() {
        setUrl("inflectraUrl");
        BrowserUtils.waitForPageToLoad(Duration.ofSeconds(3));
        assertTrue(Driver.get().getTitle().contains("Inflectra"));
    }

    /**
     * The testSearchFunctionality() method verifies the functionality of the search feature.
     * It enters a search term, performs a search, and validates the search results page URL.
     *
     * @throws InterruptedException if the thread is interrupted while waiting for the page to load
     */
    @Test
    public void testSearchFunctionality() throws InterruptedException {
        String searchWord = "Test";
        setUrl("inflectraUrl");
        homePage.getSearchButton().click();
        WebElement searchInput = homePage.getSearchInput();
        searchInput.sendKeys(searchWord, Keys.ENTER);
        BrowserUtils.waitForPageToLoad(Duration.ofSeconds(20));
        assertThat(Driver.get().getCurrentUrl()).contains(searchWord).isNotEmpty();
    }
    /**
     * The testLogin() method verifies the login functionality.
     * It navigates to the Inflectra URL, logs in, and validates the resulting page.
     */
    @Test
    public void testLogin() {
        setUrl("inflectraUrl");
        homePage.getLoginButtonNav().click();
        loginPage.getLoginForm();
        feelLoginFormViaJS();
        loginPage.getLoginSubmitButton().click();
        assertThat(Driver.get().getCurrentUrl()).contains("Customer").isNotEmpty();
        String title = accountPage.getPageTitle().getText();
        assertThat(title).isEqualTo("Customer Area "  + getTestUserData("userName") + " ()");
    }

    /**
     * The testSignUpforTrial() method verifies the functionality of signing up for a trial.
     * It navigates to the Inflectra URL, clicks on the trial button in the navigation bar, and validates the trial sign-up form.
     */
    @Test
    public void testSignUpforTrial() {
        setUrl("inflectraUrl");
        homePage.getTrialButtonNav().click();
        trialInfoPage.getTrialGoButton().click();
        WebElement trialRegisterForm = trialSignUpPage.getTrialSignUpForm();
        assertThat(trialRegisterForm).isNotNull();
    }
    /**
     * The testContactGermanyInfo() method verifies the contact information for Germany (Sales) on the Contact Us page.
     */
    @Test
    public void testContactGermanyInfo() {
        setUrl("inflectraUrl");
        homePage.getContactUsButtonNav().click();
        String searchInfoGe = contactUsPage.getContactInfoGe().getText();
        assertThat(searchInfoGe).isEqualTo("Germany (Sales)");
    }
//
    /**
     * search functionality test on home page with Actions
     * blocked
     */
//    @Test
//    public void testSearchFunctionalityAction() throws InterruptedException {
//        Actions action = new Actions(Driver.get());
//        setUrl("inflectraUrl");
//        WebElement searchIcon = homePage.getSearchButton();
//        action.moveToElement(searchIcon).click().perform();
//        WebElement searchInputField = homePage.getSearchInput();
//        String searchWord = "Test";
//        action.moveToElement(searchInputField).click().sendKeys(searchWord).sendKeys(Keys.ENTER).build().perform();
//        assertThat(Driver.get().getCurrentUrl()).contains(searchWord).isNotEmpty();
//    }

    /**
     * Fills the login form with user data using JavaScript.
     * It retrieves the WebDriver instance, casts it to JavascriptExecutor, and executes JavaScript code
     * to set the email and password fields in the login form.
     */
    private void feelLoginFormViaJS() {
        JavascriptExecutor js = (JavascriptExecutor) Driver.get();
        js.executeScript("document.getElementById('cplMainContent_LoginUser_UserName').value = arguments[0];", getTestUserData("email"));
        js.executeScript("document.getElementById('cplMainContent_LoginUser_Password').value = arguments[0];", getTestUserData("password"));
    }
}
